#!/usr/bin/env bash

# Update just for good measure
sudo yum update -y

# Git Configuration
name="Neil Millard"
email="neil@neilmillard.com"

config="
[user]
	name=$name
	email=$email"

echo "$config" > /home/vagrant/.gitconfig

# Install git
sudo yum install git -y

# Install node.js and npm
sudo yum install nodejs-legacy -y
sudo yum install npm -y

# Install bower
npm install -g bower

# Install http-server
npm install -g http-server

# Install ember-cli
npm install -g ember-cli

# Copy aliases
cp /vagrant/aliases /home/vagrant/.bash_aliases
